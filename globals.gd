extends Node

var player
var hud
var weapon
var game

func move_node(node, to):
	var pos = node.get_global_transform()
	var parent = node.get_parent()
	parent.remove_child(node)
	to.add_child(node)
	node.set_global_transform(pos)

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
