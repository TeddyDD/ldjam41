extends KinematicBody2D

export var speed = 500
var velocity = Vector2()
var direction

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	velocity = direction.normalized() * speed
#	velocity.x = speed
#	velocity.rotated(rotation_degrees)
	var c = move_and_collide(velocity * delta)
	if c != null:
		queue_free()
