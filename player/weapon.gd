extends Position2D

# weapons stack
var weapons = []

var prev_clicked = false
var clicked = false

func _input(event):
	clicked = false
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		var now = event.pressed
		if not prev_clicked and now:
			clicked = true
		prev_clicked = now
		return
	prev_clicked = false

func _ready():
	globals.weapon = self
	
	# default simple weapon
	var w = DefaultWeapon.new()
	w.root = self
	weapons.append(w)
	
	pass

func _process(delta):
	look_at(get_global_mouse_position())
	if clicked:
		clicked = false
		prev_clicked = true
		weapons.back().activate()
		weapons.back().process(delta)
	
class Weapon:
	func activate():
		pass
	func preview():
		pass
	func process(delta):
		pass
		
class DefaultWeapon:
	extends Weapon
	var root
	var cooldown = 0.3
	var time_left = 0
	var scene = preload("res://weapons/simple.tscn")
	func activate():
		if time_left <= 0:
			time_left = cooldown
			var p = scene.instance()
			p.position = root.get_node("shoot").global_position
			p.rotation = root.rotation
			p.direction = root.get_global_mouse_position() - root.global_position
			globals.game.add_child(p)
	func process(delta):
		if time_left < 0:
			time_left = time_left - delta
		else:
			time_left = 0
		
	
	
	
	
	