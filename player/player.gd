extends KinematicBody2D

var lazy = preload("res://addons/lazy_move/lazy_move.gd")
var stateMachine = preload("res://addons/tilde-toolkit/fsm/FSM.gd")
var fsm

# consts

var gravity = Vector2( 0, 400 )
var velocity = Vector2()
var on_floor = false
var dir = Vector2()

var prev_anim = "idle"
var prev_anim_back = false

func set_anim(name, reversed=false):
	if name == prev_anim and reversed==prev_anim_back:
		return
#	if not reversed:
	$AnimationPlayer.play(name)
#	else:
#		$AnimationPlayer.play_backwards(name)
	prev_anim = name
	prev_anim_back = reversed

func _ready():
	globals.player = self
	setup_fsm()
	
func setup_fsm():
	# fsm
	fsm = stateMachine.new_root("player", self)
	
	# states
	var idle = StateIdle.new()
	idle.name = "idle"
	
	var walk = StateWalk.new()
	walk.name = "walk"
	
	var jump = StateJump.new()
	jump.name = "jump"
	
	fsm.add_state(idle)
	fsm.add_state(walk)
	fsm.add_state(jump)
	
	# transitions
	var move_pressed = TransitionMovePressed.new()
	var nothing_pressed = TransitionNoMovePressed.new()
	var jump_pressed = TransitionJump.new()
	var landed = TransitionLanded.new()
	
	idle.add_transition("walk", move_pressed)
	walk.add_transition("idle", nothing_pressed)
	
	idle.add_transition("jump", jump_pressed)
	walk.add_transition("jump", jump_pressed)
	
	jump.add_transition("idle", landed)
#	jump.add_transition("walk", move_pressed)
#	jump.add_transition("idle", nothing_pressed)
	
	fsm.enter(null)

func _physics_process(delta):
	# direction
	dir = get_local_mouse_position()
	dir = dir.normalized()
	
	if dir.x > 0:
		scale.x = 1
#		set_anim(prev_anim, false)
#		$Sprite.flip_h = false
		pass
	else:
#		set_anim(prev_anim, true)
		scale.x = -1
#		$Sprite.flip_h = true
		pass
	
	fsm.process(delta)
	$Label.text = fsm._current_state

class StateIdle:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	func process(delta):
		root.velocity.y = root.gravity.y
		if root.velocity.x > 0:
			root.set_anim("idle", false)
		else:
			root.set_anim("idle", true)
		root.move_and_slide(root.velocity, Vector2(0, -1))
		root.on_floor = root.is_on_floor()
#		root.move_and_collide(root.gravity * delta)

class StateWalk:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	var WALK_SPEED = 400
	func process(delta):
		var l = root.lazy.ui_direction()
		root.velocity.x = WALK_SPEED * l.x
		root.velocity.y = root.gravity.y
		if root.velocity.x > 0:
			root.set_anim("run", false)
		else:
			root.set_anim("run", true)
		root.move_and_slide(root.velocity, Vector2(0, -1))
		root.on_floor = root.is_on_floor()

# Jumping
class StateJump:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	var JUMP_FORCE = 800
	var WALK_SPEED = 300
	var on_floor = false
	func enter(from):
		root.velocity.y = -JUMP_FORCE
		on_floor = false
	func process(delta):
		var l = root.lazy.ui_direction()
		root.velocity.x = WALK_SPEED * l.x
		if root.velocity.y < root.gravity.y:
			root.velocity.y += root.gravity.y * delta * 3
		root.move_and_slide(root.velocity, Vector2(0, -1))
		if root.is_on_ceiling():
			root.velocity.y = 0
		root.on_floor = root.is_on_floor()


class TransitionMovePressed:
	func check(from, to):
		var d = from.root.lazy.ui_direction()
		if d != Vector2() and d.y == 0:
			return true
		return false
		
class TransitionJump:
	func check(from, to):
		var d = from.root.lazy.ui_direction()
		if d != Vector2() and d.y < 0 and from.root.on_floor:
			return true
		return false
		
class TransitionNoMovePressed:
	func check(from, to):
		if from.root.lazy.ui_direction() == Vector2():
			return true
		return false
		
class TransitionLanded:
	func check(from, to):
		if from.root.on_floor:
			return true
		return false
			
class TransitionTrue:
	func check(from, to):
		return true