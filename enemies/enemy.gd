extends KinematicBody2D

export var patrol = true
var stateMachine = preload("res://addons/tilde-toolkit/fsm/FSM.gd")
var fsm
var direction = Vector2(1,0)
var velocity = Vector2()
var gravity = Vector2(0, 300)

func _ready():
	randomize()
	setup_fsm()
	
func setup_fsm():
	fsm = stateMachine.new_root("enemy", self)
	
	var patrol = Patrol.new()
	patrol.name = "patrol"
	
	var swap = SwapDirection.new()
	swap.name = "swap"
	
	fsm.add_state(patrol)
	fsm.add_state(swap)
	
	var nofloor = NoFloor.new()
	var then = Then.new()
	
	patrol.add_transition("swap", nofloor)
	swap.add_transition("patrol", then)
	
	fsm.enter(null)
	
func randomize_direction():
	if randi() % 2 == 0:
		set_direction(1)
	else:
		set_direction(-1)
		
func add_gravity():
	velocity.y = gravity.y
	
func set_direction(x):
	direction.x = x
	$Sprite.flip_h = false if x > 0 else true
	$fall.cast_to = Vector2(30 * x, 40)
	
func _physics_process(delta):
	$Label.text = fsm._current_state
	fsm.process(delta)

class Patrol:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	var speed = 200
	var raycast_once = false
	
	func enter(from):
		root.randomize_direction()
		
	func process(delta):
		root.velocity = Vector2(1,0) * root.direction * speed
		root.add_gravity()
		
		root.move_and_slide(root.velocity)
		
class SwapDirection:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	func enter(from):
		root.set_direction(root.direction.x * -1)

class NoFloor:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		return not from.root.get_node("fall").is_colliding()
		
class Then:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		return true