extends Node2D

var stateMachine = preload("res://addons/tilde-toolkit/fsm/FSM.gd")
var fsm

var pickup_area
var mouse_in = false

var prev_clicked = false
var clicked = false

func _ready():
	setup_fsm()

func _input(event):
	clicked = false
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		var now = event.pressed
		if not prev_clicked and now and mouse_in:
			clicked = true
		prev_clicked = now
		return
	prev_clicked = false

func setup_fsm():
	fsm = stateMachine.new_root("card_fsm", self)
	
	# states
	var on_ground = StateOnGround.new()
	on_ground.name = "on_ground"
	
	var to_inventory = StateToInventory.new()
	to_inventory.name = "to_inventory"
	
	var in_inventory = StateInInventory.new()
	in_inventory.name = "in_inventory"
	
	var activated = StateActivated.new()
	activated.name = "activated"
	
	fsm.add_state(on_ground)
	fsm.add_state(to_inventory)
	fsm.add_state(in_inventory)
	fsm.add_state(activated)
	
	# transitions
	var pick = TransitionPickedUp.new()
	var tween_done = TransitionTweenDone.new()
	var clicked = TransitionClicked.new()
	
	on_ground.add_transition("to_inventory", pick)
	to_inventory.add_transition("in_inventory", tween_done)
	in_inventory.add_transition("activated", clicked)
	
	fsm.enter(null)
	

func _process(delta):

	fsm.process(delta)

class StateOnGround:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	func enter(from):
		root.get_node("Sprite").scale = Vector2(0.5, 0.5)
		root.get_node("AnimationPlayer").play("floating")
		
class StateToInventory:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	var slot
	var finished
	var tween
	var FLY_TIME = 0.5
	func enter(from):
		root.hide()
		finished = false
		slot.empty = false
		
		tween = root.get_node("Tween")
		
		tween.connect("tween_completed", self, "tween_done")
		# pos
		var old_trans = root.get_canvas_transform()
		tween.stop_all()
		globals.move_node(root, slot.ref)
		tween.interpolate_property(
		root, "global_position",
		root.global_position + old_trans.origin,
		slot.ref.global_position,
		FLY_TIME, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT )
		# rescale
		root.get_node("AnimationPlayer").play("default")
		tween.interpolate_property(root.get_node("Sprite"), "scale", Vector2(0.5, 0.5), Vector2(1,1),
		FLY_TIME, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		
		tween.start()
		
	func exit(to):
		tween.disconnect("tween_completed", self, "tween_done")
		
	func tween_done(obj, key):
		finished = true
		
class StateInInventory:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	var slot
	func enter(from):
		slot = from.slot

class StateActivated:
	extends "res://addons/tilde-toolkit/fsm/State.gd"
	func enter(from):
		var slot = from.slot
		slot.empty = true
		# TODO ACTIVATE CARD
		root.queue_free()
	
class TransitionPickedUp:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		if from.root.pickup_area != null:
			var slot = globals.hud.empty_slot()
			if slot != null:
				to.slot = slot
				return true
		return false

class TransitionTweenDone:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from,to):
		return from.finished
		
class TransitionClicked:
	extends "res://addons/tilde-toolkit/fsm/Transition.gd"
	func check(from, to):
		if from.root.mouse_in == true and from.root.clicked:
			return true
		return false

func _on_pickup_area_entered(area):
	prints("area enter")
	pickup_area = area

func _on_moues_pick_mouse_entered():
	mouse_in = true
	if fsm._current_state == "in_inventory":
		var tween = get_node("Tween")
		tween.interpolate_property(self, "scale", Vector2(1,1), Vector2(1.5, 1.5),
		0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()

func _on_moues_pick_mouse_exited():
	mouse_in = false
	if fsm._current_state == "in_inventory":
		var tween = get_node("Tween")
		tween.interpolate_property(self, "scale", scale, Vector2(1, 1),
		0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()

func _on_Tween_tween_step(object, key, elapsed, value):
	if ( not object.visible and key==':global_position' and elapsed>=0.0 ):
		object.show()
