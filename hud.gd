extends CanvasLayer

var slots = []

func _ready():
	globals.hud = self
	for i in range(1,6):
		var r = {
			ref = get_node("slot %d" % i),
			empty = true
		}
		slots.append(r)
		
# Return first empty slot or null if eq is full
func empty_slot():
	for i in range(slots.size()):
		if slots[i].empty == true:
			return slots[i]
	return null

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
